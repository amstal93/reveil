# Development Guide

## Build Docker locally

### Step 1 /  Build Frontend
```bash
chmod +x ./build-frontend.sh
```

This will build the frontend and add it to `publish/frontend/` directory.

### Step 2 / Build Dockerfile
```bash
docker build -t reveil:latest .
```

This will build the Dockerfile. The Dockerfile builds the Backend, and copies
everything within `publish/frontend`-directory into its `wwwroot`-directory. This step is called "bundling".

### Step 3 / Run Docker Container
```bash
docker run \
  -p 8080:8080 \
  -e Kestrel__EndPoints__Http__Url='http://0.0.0.0:8080' \
  -e FullyQualifiedDomainName=localhost \
  -e RedirectToHttps=false 
  reveil:latest
```

this will expose HTTP Listening Port on 8080. With `RedirectToHttps` set to false, no requests will be redirected to HTTPs.


