using System.Net;
using CSharpFunctionalExtensions;
using FluentAssertions;
using Moq;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.ContentBl {
  public class GetContentTests {
    [Theory]
    [InlineData(nameof(GetContentTests), "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("F", nameof(GetContentTests), RepoType.InMemory)]
    [InlineData("F", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData(nameof(GetContentTests), "0123456789ABCDEF", RepoType.FilePersisted)]
    [InlineData("F", nameof(GetContentTests), RepoType.FilePersisted)]
    [InlineData("F", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestNotExistingContentReturns404ErrorResponse(string contentType, string contentLang, RepoType repoType) {
      var repo = RepositoryFactory.Create<ContentEntity>(repoType);
      var bl = new Backend.BusinessLogic.ContentBl(repo);
      var r = bl.GetContent(contentType, contentLang);
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
      repo.Remove(_ => true);
    }

    [Theory]
    [InlineData("", "0123456789ABCDEF")]
    [InlineData("0123456789ABCDEF", "")]
    public void TestFailingRepoOperatingReturns500ErrorResponse(string contentType, string contentLang) {
      const string Error = "Database Error";
      var mock = new Mock<IGenericRepository<ContentEntity>>();
      mock
        .Setup(x => x.Get(It.IsAny<string>()))
        .Returns(() => Result.Failure<Maybe<ContentEntity>>(Error));

      var bl = new Backend.BusinessLogic.ContentBl(mock.Object);

      var r = bl.GetContent(contentType, contentLang);
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.Error.Should().Contain(Error);
      r.Error.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
    }

    [Theory]
    [InlineData(nameof(GetContentTests), "012", RepoType.InMemory)]
    [InlineData("012", nameof(GetContentTests), RepoType.InMemory)]
    [InlineData("012", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData(nameof(GetContentTests), "0123456789ABCDEF", RepoType.FilePersisted)]
    [InlineData("012", nameof(GetContentTests), RepoType.FilePersisted)]
    [InlineData("012", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestExistingContentEntityReturnsSuccessful200OkResponse(string contentType, string language, RepoType repoType) {
      var id = $"{contentType}-{language}";
      const string Content = "Hello";
      var repo = RepositoryFactory.Create<ContentEntity>(repoType);
      repo.Add(new ContentEntity {Content = Content, Id = id, Language = language, Type = contentType});
      var bl = new Backend.BusinessLogic.ContentBl(repo);

      var r = bl.GetContent(contentType, language);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<ContentEntryResponse>();
      r.Value.Id.Should().Be(id);
      r.Value.Content.Should().Be(Content);
      r.Value.Language.Should().Be(language);
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);

      repo.Remove(_ => true);
    }
  }
}
