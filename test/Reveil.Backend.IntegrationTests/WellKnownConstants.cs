using System;

namespace Reveil.Backend.IntegrationTests {
  public static class WellKnownConstants {
    public static readonly string IntegrationTestUserId = Guid.Empty.ToString();
    public static readonly string IntegrationTestAuthorizationToken = Guid.Empty.ToString();
  }
}
