FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["src/Reveil.Backend/Reveil.Backend.csproj", "Reveil.Backend/"]
RUN dotnet restore "/src/Reveil.Backend/Reveil.Backend.csproj"
COPY ./src/Reveil.Backend/ ./Reveil.Backend/
WORKDIR "/src/Reveil.Backend"
RUN dotnet build "Reveil.Backend.csproj" -c Release -o /app/build

FROM build AS publish
COPY ["publish/frontend/", "/app/publish/wwwroot"]
RUN dotnet publish "Reveil.Backend.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Reveil.Backend.dll"]
