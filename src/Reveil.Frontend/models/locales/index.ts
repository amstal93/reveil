import { IModelLocalesCCode } from './ccode'
import { IModelLocalesLCode } from './lcode'
import { IModelLocalesTCode } from './tcode'

export { IModelLocalesCCode, IModelLocalesLCode, IModelLocalesTCode }
