import * as api from '~/api'

import { IModelFile, IModelFileData, IModelFileResponse, IModelFileDataPayload } from './models'

import { RndGen } from '~/tsmodules/rndgen'
import { Base64 } from '~/tsmodules/base64'

import { ApiFileMeta } from '~/tsmodules/api/filemeta'

import { ApiEndpointExtensionLayer, ApiEndpointExtensionExtend } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'


export class ApiFile extends ApiEndpointExtensionExtend {
    constructor(eel: ApiEndpointExtensionLayer) {
        super(eel)
    }

    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    // >!< file
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    async FileExists(case_id: string, message_id: string, file_id: string): Promise<boolean> {
        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getFileMetadata(case_id, message_id, file_id)
        .then(() => {
            return true;
        })
        .catch((e) => {
            return false;
        })
    }

    async FileGet(case_id: string, message_id: string, file_id: string): Promise<IModelFile> {
        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getFileMetadata(case_id, message_id, file_id)
        .then(async (e) => {
            var rtvl = {} as IModelFile
            rtvl.response = e.data
            return rtvl;
        })
        .catch((e) => {
            throw Error('ModCase => file_get')
        })
    }

    async FileDataGet(case_id: string, message_id: string, file_id: string): Promise<IModelFileData> {
        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getFileContent(case_id, message_id, file_id)
        .then(async (e) => {
            var rtvl = {
                response: e.data,
                payload: {
                    data: ApiCrypt.Proxy(ApiCrypt.ApiCryptTypeByIndex(e.data.dataProcessingMethodHint!), this.eel!).Decrypt(e.data.data!),
                } as IModelFileDataPayload,
            } as IModelFileData
            rtvl.response = e.data

            return rtvl;
        })
        .catch((e) => {
            throw Error('ModCase => filedata_get')
        })
    }

    async FileGetAll(case_id: string, message_id: string): Promise<Array<IModelFile>> {
        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getFiles(case_id, message_id)
        .then((crl) => {
            var rtvl = []
            for(var file_id of crl.data.ids || []) {
                rtvl.push(this.FileGet(case_id, message_id, file_id).then((e) => {
                    return e
                }))
            }
            return Promise.all(rtvl)
        })
        .catch((e) => {
            throw Error('ModCase => file_getall')
        })
    }

    async FilePost(case_id: string, message_id: string, file_id: string | null, file: File): Promise<IModelFileResponse> {
        // file_id :: autogen uuid
        if(file_id === null) {
            file_id = new RndGen().UUIDv4()
        }

        // upload & meta & crypt
        var payload_data = ""
        payload_data = await this.FileUpload(file)
        payload_data = ApiFileMeta.WipeMeta(file, payload_data, this.eel!)
        payload_data = ApiCrypt.Proxy<string>(this.eel?.CryptType!, this.eel!).Encrypt(payload_data)
        var payload = {
            data: payload_data,
            fileName: file.name,
            publicKey: null,
            dataProcessingMethodHint: this.eel?.CryptType.toString()
        } as api.FileUploadRequest

        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).uploadFile(case_id, message_id, file_id, payload)
        .then((e) => {
            return {
                response: e.data,
                ref_obj: file_id,
            } as IModelFileResponse
        })
        .catch((e) => {
            throw Error("ModCase => file_post")
        })
    }

    async FilePostAll(case_id: string, message_id: string, files: Array<File>): Promise<Array<IModelFileResponse>> {
        var rtvl = [] as Array<Promise<IModelFileResponse>>
        
        for(var file of files) {
            rtvl.push(this.FilePost(case_id, message_id, null, file))
        }

        return Promise.all(rtvl)
        .then((e) => {
            return e
        })
    }

    async FileUpload(file: File): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            var reader = new FileReader();
            reader.onload = (e) => {
                resolve(reader.result?.toString()!)
            }
            reader.readAsDataURL(file);
        })
    }

    async FileUploadAll(files: Array<File>): Promise<Array<string>> {
        var p = [] as Array<Promise<string>>
        for(var f of files) {
            p.push(this.FileUpload(f))   
        }
        return await Promise.all(p)
        .then((e) => {
            return e
        })
    }
}
