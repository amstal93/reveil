import * as api from '~/api'

import { Pbkdf2 } from '~/tsmodules/pbkdf2'

import { IModelAuthMgmt, IModelAuthUser } from './models'
import { ApiCase } from '~/tsmodules/api/case'

import { ApiEndpointExtensionLayer, ApiEndpointExtensionExtend } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'

export class ApiAuthUser extends ApiEndpointExtensionExtend {
    constructor(eel: ApiEndpointExtensionLayer) {
        super(eel)
    }

    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    // >!< auth
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    async AuthUserLogin(username: string, password: string, remember: boolean, case_id: string = ""): Promise<IModelAuthUser | null> {
        var cache = ApiCache.Proxy<IModelAuthUser>(this.eel!.CacheType!, 'authuser', this.eel!)

        return Promise.resolve()
        .then(async (e) => {
            var model = { 
                username: username, 
                password: password, 
                remember: remember, 
                authtype: "user",
                // todo: pbkdf2 implementation is very basic. further work required.
                computed_token: new Pbkdf2("salt", 9991, 64, "sha512").Generate(password),
                computed_case_id: case_id
            } as IModelAuthUser

            cache.SetCache(model)

            // case - preflight
            if(case_id !== "") {
                var case_preflight = await new ApiCase(this.eel!).CaseExists(case_id)
                if(case_preflight === false) {
                    cache.ClearCache()
                    return null;
                    // throw Error("User is unauthorized to given case!")
                }
            }

            return model;
        })
        .catch((e) => {
            this.AuthUserLogout()
            return null
        })
    }

    AuthUserState(): IModelAuthUser | null {
        return ApiCache.Proxy<IModelAuthUser>(this.eel!.CacheType!, 'authuser', this.eel!).GetCache()
    }

    async AuthUserLogout(): Promise<boolean> {
        ApiCache.Proxy<IModelAuthMgmt>(this.eel!.CacheType!, 'authuser', this.eel!).ClearCache()
        return true;
    }
}
