import { ApiFileMetaIMod } from './imod'
import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

import { stripexif } from './jpeg-lib'

export class ApiFileMetaModsJPEG implements ApiFileMetaIMod {
    public static WipeMeta(file: File, content: string, inject: ApiEndpointExtensionLayer): string {
        try {
            return stripexif(content)
            // return pxfy.remove(content)
            return content
        }
        catch(e) {
            return content
        }
    }
}
