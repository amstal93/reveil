import { Middleware } from '@nuxt/types'
import { ApiAuthMgmt, ApiAuthUser } from '~/tsmodules/api/auth'

const auth: Middleware = async (context) => {
    // access user-ressources :: unauthorized
    if(context.route.path.startsWith("/user/") && context.$tsmapi.authuser().AuthUserState() === null) {
        await context.$tsmapi.authmgmt().AuthMgmtLogout()
        context.redirect('/login/user');
    }

    // access mgmt-ressources :: unauthorized
    if(context.route.path.startsWith("/mgmt/") && context.$tsmapi.authmgmt().AuthMgmtState() === null) {
        await context.$tsmapi.authuser().AuthUserLogout()
        context.redirect('/login/mgmt');
    }
}

export default auth
