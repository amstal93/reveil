import { Plugin, Context } from '@nuxt/types'

import * as tsmapi_ from '~/tsmodules/api'
import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'

// >>> typing
interface ITsmApi {
    eel(
        override_bootstrap_type?: ApiBootstrapType | null,
        override_cache_type?: ApiCacheType | null,
        override_crypt_type?: ApiCryptType | null,
        override_context?: Context | null,
    ): ApiEndpointExtensionLayer
    authuser(override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiAuthUser
    authmgmt(override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiAuthMgmt
    file(override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiFile
    case(override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiCase
    message(override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiMessage
    journal(override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiJournal
    content(override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiContent
    bootstrap(override_eel?: ApiEndpointExtensionLayer | null): ApiBootstrapModsIMod
    cache<T>(name: string, override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiCache
}

// >>> data
export var data = function(context: Context): ITsmApi {
    return {
        eel(
            override_bootstrap_type?: ApiBootstrapType | null,
            override_cache_type?: ApiCacheType | null,
            override_crypt_type?: ApiCryptType | null,
            override_context?: Context,
        ): ApiEndpointExtensionLayer {
            return new ApiEndpointExtensionLayer(
                override_bootstrap_type || null,
                override_cache_type || null,
                override_crypt_type || null,
                override_context || context)
        },
        authuser(override_eel?: ApiEndpointExtensionLayer | null): tsmapi_.ApiAuthUser {
            return new tsmapi_.ApiAuthUser(
                override_eel || this.eel()
            )
        },
        authmgmt(override_eel: ApiEndpointExtensionLayer | null): tsmapi_.ApiAuthMgmt {
            return new tsmapi_.ApiAuthMgmt(
                override_eel || this.eel()
            )
        },
        file(override_eel: ApiEndpointExtensionLayer | null): tsmapi_.ApiFile {
            return new tsmapi_.ApiFile(
                override_eel || this.eel()
            )
        },
        case(override_eel: ApiEndpointExtensionLayer | null): tsmapi_.ApiCase {
            return new tsmapi_.ApiCase(
                override_eel || this.eel()
            )
        },
        message(override_eel: ApiEndpointExtensionLayer | null): tsmapi_.ApiMessage {
            return new tsmapi_.ApiMessage(
                override_eel || this.eel()
            )
        },
        journal(override_eel: ApiEndpointExtensionLayer | null): tsmapi_.ApiJournal {
            return new tsmapi_.ApiJournal(
                override_eel || this.eel()
            )
        },
        content(override_eel: ApiEndpointExtensionLayer | null): tsmapi_.ApiContent {
            return new tsmapi_.ApiContent(
                override_eel || this.eel()
            )
        },

        bootstrap(override_eel: ApiEndpointExtensionLayer | null): ApiBootstrapModsIMod {
            return ApiBootstrap.Proxy((override_eel || this.eel()).BootstrapType, override_eel || this.eel())
        },
        cache<T>(name: string, override_eel: ApiEndpointExtensionLayer | null): tsmapi_.ApiCache {
            return tsmapi_.ApiCache.Proxy<T>((override_eel || this.eel()).CacheType, name, override_eel || this.eel()) 
        },
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $tsmapi: ITsmApi
    }
}

declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $tsmapi: ITsmApi
    }
    
    interface Context {
        $tsmapi: ITsmApi
    }
}

const tsmapi: Plugin = (context, inject) => {
    inject('tsmapi', data(context))
}

export default tsmapi
