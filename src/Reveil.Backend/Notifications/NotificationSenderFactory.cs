using System;
using System.Linq;
using Microsoft.Extensions.Options;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Configuration.Notification;
using Reveil.Backend.Notifications.Mail;
using Reveil.Backend.Notifications.Webhooks.Discord;

namespace Reveil.Backend.Notifications {
  public class NotificationSenderFactory : INotificationSenderFactory {
    private readonly IOptions<NotificationConfiguration> _configuration;
    public NotificationSenderFactory(IOptions<NotificationConfiguration> configuration) {
      _configuration = configuration;
    }
    public INotificationSender Create() {
      if (_configuration.Value.Targets.Count == 0)
        return new DevNullSender();
      
      var senders = _configuration.Value.Targets
        .Select(Map)
        .ToList();

      return _configuration.Value.Targets.Count > 1
        ? CombinedNotificationTarget.CreateCombined(senders)
        : senders.First();
    }

    private static INotificationSender Map(NotificationTargetConfigurationBase configurationBase) {
      return configurationBase switch {
        DiscordNotificationTargetConfiguration c => new DiscordNotifier(c),
        SmtpNotificationTargetConfiguration c => new SmtpNotifier(c),
        _ => throw new NotImplementedException("Notification Target Configuration exists, but the corresponding implementation does not (yet) exist, or is not mapped")
      };
    }
  }
}
