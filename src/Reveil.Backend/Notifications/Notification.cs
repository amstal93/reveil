﻿namespace Reveil.Backend.Notifications {
  public class Notification {
    public string Message { get; set; }
    public string CaseId { get; set; }
    public NotificationType NotificationType { get; set; }
  }
}
