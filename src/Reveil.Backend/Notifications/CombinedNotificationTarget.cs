using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Reveil.Backend.Interfaces;

namespace Reveil.Backend.Notifications {
  public class CombinedNotificationTarget : INotificationSender {
    private readonly IList<INotificationSender> _senders;
    public string Notifier => "Combined";

    private CombinedNotificationTarget(IList<INotificationSender> senders) {
      _senders = senders;
    }
    
    public async Task<Result<Notification, NotificationError>> SendAsync(Notification notification) {
      var tasks = _senders.Select(s => s.SendAsync(notification));
      var results = await Task.WhenAll(tasks);
      
      return results.Any(r => r.IsFailure)
        ? results.First(r => r.IsFailure)
        : results.First(r => r.IsSuccess);
    }

    public static INotificationSender CreateCombined(IList<INotificationSender> senders) {
      return new CombinedNotificationTarget(senders);
    }
  }
}
