using System;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using MailKit.Net.Smtp;
using MimeKit;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Configuration.Notification;

namespace Reveil.Backend.Notifications.Mail {
  public class SmtpNotifier : INotificationSender {
    private readonly SmtpNotificationTargetConfiguration _smtpNotificationTargetConfiguration;
    public string Notifier => "Smtp";

    public SmtpNotifier(SmtpNotificationTargetConfiguration smtpNotificationTargetConfiguration) {
      _smtpNotificationTargetConfiguration = smtpNotificationTargetConfiguration;
    }

    public async Task<Result<Notification, NotificationError>> SendAsync(Notification notification) {
      try {
        await SendMailMessage(CreateMailMessage(notification));
        return Result.Success<Notification, NotificationError>(notification);
      } catch (Exception) {
        return Result.Failure<Notification, NotificationError>(new NotificationError());
      }
    }

    private async Task SendMailMessage(MimeMessage message) {
      using var client = new SmtpClient();
      await client.ConnectAsync(_smtpNotificationTargetConfiguration.SmtpServer, _smtpNotificationTargetConfiguration.SmtpPort, _smtpNotificationTargetConfiguration.UseSsl);

      if (_smtpNotificationTargetConfiguration.UseSmtpAuthentication) {
        await client.AuthenticateAsync(_smtpNotificationTargetConfiguration.SmtpServerUsername, _smtpNotificationTargetConfiguration.SmtpServerPassword);
      }

      await client.SendAsync(message);
      await client.DisconnectAsync(true);
    }

    private MimeMessage CreateMailMessage(Notification notification) {
      var message = new MimeMessage();
      var sender = new MailboxAddress(_smtpNotificationTargetConfiguration.SenderName, _smtpNotificationTargetConfiguration.SenderEmail);
      message.From.Add(sender);

      _smtpNotificationTargetConfiguration
        .Recipients
        .ForEach(r => message.Bcc.Add(new MailboxAddress(r.RecipientName, r.RecipientEmail)));

      message.Subject = $"Reveil {notification.NotificationType.ToString()}"; // TBD: Localize
      message.Body = new TextPart("plain") {Text = notification.Message};
      return message;
    }
  }
}
