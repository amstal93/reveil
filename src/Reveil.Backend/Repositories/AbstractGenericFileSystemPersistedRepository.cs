using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using CSharpFunctionalExtensions;
using Microsoft.Extensions.Options;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Repositories {
  public abstract class AbstractGenericFileSystemPersistedRepository<T> : IGenericRepository<T> where T : BaseEntity {
    private readonly IOptions<PersistenceConfiguration> _persistenceConfiguration;

    protected AbstractGenericFileSystemPersistedRepository(IOptions<PersistenceConfiguration> persistenceConfiguration) {
      _persistenceConfiguration = persistenceConfiguration;
    }

    protected virtual Result WriteToIndex(string id, string path) {
      try {
        Directory.CreateDirectory(GetIndexBasePath());
        File.WriteAllText(BuildIndexPath(id), path, Encoding.UTF8);
        return Result.Success();
      } catch (Exception exc) {
        return Result.Failure($"Could not write to Index because {exc.Message}");
      }
    }

    protected virtual Result<Maybe<T>> ParseDataFile(string filePath) {
      try {
        if (!File.Exists(filePath))
          return Result.Failure<Maybe<T>>("Inconsistent Data Storage. Index exists for Id {id}. But Object does not");
        var content = File.ReadAllText(filePath, Encoding.UTF8);
        var data = JsonSerializer.Deserialize<T>(content);
        return Result.Success(Maybe.From(data!));
      } catch (Exception exc) {
        return Result.Failure<Maybe<T>>(exc.Message);
      }
    }

    protected virtual Result<Maybe<T>> LookupFromIndex(string id) {
      try {
        var indexFile = BuildIndexPath(id);
        if (!File.Exists(indexFile))
          return Result.Success(Maybe<T>.None);
        var dataPath = File.ReadAllText(indexFile, Encoding.UTF8);
        return ParseDataFile(dataPath);
      } catch (Exception exc) {
        return Result.Failure<Maybe<T>>(exc.Message);
      }
    }

    protected virtual Result RemoveFromIndex(string id) {
      try {
        var indexPath = BuildIndexPath(id);
        if (!File.Exists(indexPath))
          return Result.Success();
        File.Delete(indexPath);
        return Result.Success();
      } catch (Exception exc) {
        return Result.Failure(exc.Message);
      }
    }

    public virtual Result<T> Add(T entity) {
      try {
        var dataPath = GetDataPath(entity);
        if (File.Exists(dataPath))
          return Result.Failure<T>("Entity does already exist");

        var content = JsonSerializer.Serialize(entity);
        File.WriteAllText(dataPath, content, Encoding.UTF8);
        return WriteToIndex(entity.Id, dataPath).Map(() => entity);
      } catch (Exception exc) {
        return Result.Failure<T>(exc.Message);
      }
    }

    public virtual Result<Maybe<T>> Get(string id) {
      return LookupFromIndex(id);
    }

    public virtual Result<Maybe<T>> Get(Predicate<T> predicate) {
      var val = Directory.GetFiles(GetIndexBasePath(), "*.idx", SearchOption.TopDirectoryOnly)
        .Select(File.ReadAllText)
        .Select(ParseDataFile)
        .Where(r => r.IsSuccess && r.Value.HasValue)
        .Select(r => r.Value.Value)
        .FirstOrDefault(r => predicate(r));
      return val == null
        ? Result.Success(Maybe<T>.None)
        : Maybe.From(val);
    }

    public virtual Result<IList<T>> GetAll(Predicate<T> predicate) {
      try {
        return Result.Success<IList<T>>(
          Directory.GetFiles(GetIndexBasePath(), "*.idx", SearchOption.TopDirectoryOnly)
            .Select(File.ReadAllText)
            .Select(ParseDataFile)
            .Where(r => r.IsSuccess && r.Value.HasValue)
            .Select(r => r.Value.Value)
            .Where(p => predicate(p))
            .ToList());
      } catch (Exception exc) {
        return Result.Failure<IList<T>>(exc.Message);
      }
    }

    public virtual Result Remove(Predicate<T> predicate) {
      return Result.Combine(
        Directory.GetFiles(GetIndexBasePath(), "*.idx", SearchOption.TopDirectoryOnly)
          .Select(File.ReadAllText)
          .Select(ParseDataFile)
          .Where(r => r.IsSuccess && r.Value.HasValue)
          .Select(r => r.Value.Value)
          .Select(r => Remove(r.Id)));
    }

    public Result Remove(string id) {
      try {
        var indexPath = BuildIndexPath(id);
        if (!File.Exists(indexPath))
          return Result.Success();
        var dataPath = File.ReadAllText(indexPath, Encoding.UTF8);
        if (!File.Exists(dataPath))
          return Result.Success();

        File.Delete(indexPath);
        File.Delete(dataPath);

        return RemoveFromIndex(id);
      } catch (Exception exc) {
        return Result.Failure(exc.Message);
      }
    }

    protected virtual string GetDataPath(T baseEntity) {
      var basePath = Path.Join(_persistenceConfiguration.Value.DataRoot, baseEntity.GetType().Name.ToLower());
      Directory.CreateDirectory(basePath);
      return Path.Join(basePath, $"{baseEntity.Id}.json");
    }

    protected virtual string GetIndexBasePath() {
      var path = Path.Join(_persistenceConfiguration.Value.DataRoot, typeof(T).Name.ToLower(), "index");
      Directory.CreateDirectory(path);
      return path;
    }

    protected virtual string BuildIndexPath(string id) =>
      Path.Join(GetIndexBasePath(), $"{id}.idx");
  }
}
