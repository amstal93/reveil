using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Middleware.Filter;
using Reveil.Backend.Models;
using Reveil.Backend.Models.Api.Requests;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Controllers {
  [ApiController]
  [Route("[controller]")]
  [Produces("application/json")]
  [Consumes("application/json")]
  public class AuthenticationController : ControllerBase {
    private readonly IGenericRepository<AuthorizationEntity> _repo;
    private readonly IAuthenticationProviderFactory _authenticationProviderFactory;

    public AuthenticationController(IGenericRepository<AuthorizationEntity> repo, IAuthenticationProviderFactory authenticationProviderFactory) {
      _repo = repo;
      _authenticationProviderFactory = authenticationProviderFactory;
    }

    /// <summary>
    /// Authenticate a Management-User (Administrator)
    /// </summary>
    /// <param name="r">Username, Password and Nonce used for Authentication</param>
    /// <returns></returns>
    [HttpPost("/authentication/authenticate", Name = nameof(Authenticate))]
    [ProducesResponseType(typeof(AuthenticationResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public async Task<BaseResponse> Authenticate(AuthenticationRequest r) {
      var provider = _authenticationProviderFactory.Create();
      var authResult = await provider
        .AuthenticateAsync(new UserEntry {Username = r.Username, Password = r.Password});

      if (authResult.IsFailure) {
        return ErrorResponse.Create(HttpStatusCode.InternalServerError, authResult.Error);
      }

      if (authResult.IsSuccess && authResult.Value) {
        var user = await provider.GetUser(r.Username);
        if (user.IsFailure || user.Value.HasNoValue) {
          return ErrorResponse.Create(HttpStatusCode.InternalServerError, "Could not get Users Id");
        }

        var token = Guid.NewGuid().ToString();
        _repo.Add(new AuthorizationEntity {
          Id = Guid.NewGuid().ToString(),
          UserId = user.Value.Value.Id,
          AuthorizationToken = token,
          ValidTo = DateTime.UtcNow.Add(TimeSpan.FromHours(1))
        });

        return new AuthenticationResponse {
          StatusCode = HttpStatusCode.OK,
          Message = $"Welcome, {r.Username} - you're authorized",
          AuthorizationToken = token,
          DateTime = DateTime.UtcNow,
          IsSuccess = true,
          UserId = user.Value.Value.Id,
          ValidityDuration = TimeSpan.FromHours(1)
        };
      }

      return ErrorResponse.Create(HttpStatusCode.Unauthorized, "Either your Username, your password, or both were entered wrong.");
    }

    /// <summary>
    /// Refresh the validity of an already created AuthorizationToken
    /// </summary>
    /// <param name="token">the previously created authentication token</param>
    /// <returns></returns>
    [HttpPost("/authentication/refresh", Name = nameof(Refresh))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(AuthenticationResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse Refresh(string token) {
      var result = _repo.Get(t => t.AuthorizationToken == token);
      if (result.IsSuccess && result.Value.HasValue) {
        _repo.Remove(r => r.AuthorizationToken == token);
        var newToken = Guid.NewGuid().ToString();
        _repo.Add(new AuthorizationEntity {
          UserId = result.Value.Value.UserId,
          Id = Guid.NewGuid().ToString(),
          AuthorizationToken = newToken,
          ValidTo = DateTime.UtcNow.Add(TimeSpan.FromHours(1))
        });

        return new AuthenticationResponse {
          StatusCode = HttpStatusCode.OK,
          Message = "Refreshed your token",
          AuthorizationToken = newToken,
          DateTime = DateTime.UtcNow,
          IsSuccess = true,
          UserId = result.Value.Value.UserId,
          ValidityDuration = TimeSpan.FromHours(1)
        };
      }

      return ErrorResponse.Create(HttpStatusCode.Unauthorized, "Either your Username, your password, or both were entered wrong.");
    }
  }
}
