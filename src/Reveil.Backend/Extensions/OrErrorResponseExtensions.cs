using System.Net;
using CSharpFunctionalExtensions;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Extensions {
  public static class OrErrorResponseExtensions {
    public static Result<TSuccess, ErrorResponse> OrServerError<TSuccess>(this Result<TSuccess> result) {
      return result.MapError(e => ErrorResponse.Create(HttpStatusCode.InternalServerError, e));
    }

    public static Result<Maybe<TSuccess>, ErrorResponse> OrNotFound<TSuccess>(this Result<Maybe<TSuccess>, ErrorResponse> result) {
      return result.Ensure(c => c.HasValue, ErrorResponse.Create(HttpStatusCode.NotFound, $"{typeof(TSuccess).Name} not found"));
    }

    public static Result<TSuccess, ErrorResponse> MapValueOrNotFound<TSuccess>(this Result<Maybe<TSuccess>, ErrorResponse> result) {
      return result.OrNotFound().Map(m => m.GetValueOrDefault());
    }
  }
}
