using System;
using System.Linq;
using Microsoft.Extensions.Options;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Configuration.Authentication;

namespace Reveil.Backend.Authentication {
  public class AuthenticationProviderFactory : IAuthenticationProviderFactory {
    private readonly IOptions<AuthenticationConfiguration> _configuration;

    public AuthenticationProviderFactory(IOptions<AuthenticationConfiguration> configuration) {
      _configuration = configuration;
    }

    public IAuthenticationProvider Create() {
      var providers = _configuration.Value.Providers
        .Select(Map)
        .ToList();

      if (providers.Count() == 1) {
        return providers.Single();
      }

      return new AuthenticationChainOfResponsibility(providers);
    }

    private static IAuthenticationProvider Map(AuthenticationProviderConfigurationBase configurationBase) {
      return configurationBase switch {
        LdapAuthenticationProviderConfiguration c => new LdapAuthenticationProvider(c),
        StaticAuthenticationProviderConfiguration c => new StaticAuthenticationProvider(c),
        _ => throw new NotImplementedException("Provider Configuration exists, but the corresponding implementation does not (yet) exist, or is not mapped")
      };
    }
  }
}
