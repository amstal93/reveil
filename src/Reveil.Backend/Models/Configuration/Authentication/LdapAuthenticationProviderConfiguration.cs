// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Reveil.Backend.Models.Configuration.Authentication {
  public class LdapAuthenticationProviderConfiguration : AuthenticationProviderConfigurationBase {
    [Required]
    public string LdapServer { get; set; }
    [Required]
    public int LdapServerPort { get; set; } = 3890;
    [Required]
    public string LdapAdminUsername { get; set; }
    [Required]
    public string LdapAdminPassword { get; set; }

    public List<string> AllowedOUs { get; set; } = new();
    public List<string> DomainComponents { get; set; } = new();

    public string UserIdAttribute { get; set; } = "uid";
    public string MailAttribute { get; set; } = "mail";
    public string FirstNameAttribute { get; set; } = "givenName";
    public string LastNameAttribute { get; set; } = "sn";
    public string DisplayNameAttribute { get; set; } = "cn";
    public string PhoneNumberAttribute { get; set; } = "phoneNumber";
  }
}
