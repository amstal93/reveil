// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.Net;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Models.Api.Responses {
  public class ContentEntryResponse : SuccessResponse {
    public string Id { get; set; }
    public string Language { get; set; }
    public string Content { get; set; }

    public static ContentEntryResponse Create(ContentEntity entity, HttpStatusCode statusCode, string message = null) {
      var r = Create(statusCode, message);
      return new ContentEntryResponse {
        Id = entity.Id,
        Message = r.Message,
        DateTime = r.DateTime,
        IsSuccess = r.IsSuccess,
        Content = entity.Content,
        StatusCode = r.StatusCode,
        Language = entity.Language,
      };
    }
  }
}
