// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using Reveil.Backend.Models.Api.Responses.Primitives;

namespace Reveil.Backend.Models.Api.Responses {
  public class FrontendFeatureToggles {
    public bool EnableDebugMode { get; set; }
    public bool InlineFileUploadsInMessage { get; set; }
  }

  public class BootstrapConfigurationResponse {
    public string Api { get; set; }
    public string ApiPublicKey { get; set; }

    public FrontendFeatureToggles FeatureToggles { get; set; }
    public FrontendI18NConfiguration I18N { get; set; }
    public FrontendFileUploadConfiguration FileUploads { get; set; }
  }
}
