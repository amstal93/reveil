// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;
using System.Net;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Models.Api.Responses {
  public class JournalEntryResponse : SuccessResponse {
    public int Code { get; set; }
    public string Id { get; set; }
    public string CaseId { get; set; }
    public DateTime CreationDate { get; set; }

    public static JournalEntryResponse Create(JournalEntity entity, HttpStatusCode statusCode, string message = null) {
      var r = Create(statusCode, message);
      return new JournalEntryResponse {
        Id = entity.Id,
        CaseId = entity.CaseId,
        Code = (int)entity.JournalType,
        CreationDate = entity.CreationDate,
        Message = r.Message,
        DateTime = r.DateTime,
        IsSuccess = r.IsSuccess,
        StatusCode = r.StatusCode
      };
    }
  }
}
