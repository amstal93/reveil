// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Reveil.Backend.Models.Api.Responses.Primitives {
  public class AddressableId {
    public string Href { get; set; }
    public string Id { get; set; }
    public string Route { get; set; }

    public AddressableId(string id, string href, string route) {
      Id = id;
      Href = href;
      Route = route;
    }
  }
}
