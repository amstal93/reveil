using System;
using Reveil.Backend.Structures;

namespace Reveil.Backend.Models.Entities {
  public class CaseEntity : BaseEntity {
    public CaseState State { get; set; }
    public DateTime CreationDate { get; set; }
    public string AuthorizationToken { get; set; }
  }
}
