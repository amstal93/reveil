using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Reveil.Backend.Models;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Interfaces {
  public interface IAuthenticationProvider {
    Task<Result<Maybe<UserEntity>>> GetUser(string userId);

    public Task<Result<bool>> AuthenticateAsync(UserEntry entry);
  }
}
